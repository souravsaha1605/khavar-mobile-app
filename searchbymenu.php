<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Search By Menu</title>
<link rel="stylesheet" href="css/main.css">
<script>
function search(str)
{
if (str.length==0)
  { 
  document.getElementById("searchresults").innerHTML="";
  return;
  }
var xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("searchresults").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","getrestaurants.php?q="+str+"&field=MENU",true);
xmlhttp.send();
}
</script>
</head>

<body>
	
	<div id="searchbox" style="width:100%; top:0px;">
		<form >
		<input type="text" name="query" placeholder="Enter search term" style="width:68%; height:30px; float:left; ">
		<input type="button" value="Search" onclick="search(query.value);" style="width:28%; height:36px; float:right; ">
		</form>
	
	</div>
	
	<div id="searchresults" style="padding-top:40px; width=100%;"></div>

</body>
</html>