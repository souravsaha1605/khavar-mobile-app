<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Restaurant Details</title>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/lightbox-2.6.min.js"></script>

<link href="css/lightbox.css" rel="stylesheet" />
<link rel="stylesheet" href="css/main.css">
<style type="text/css">
.star-rating {
  font-size: 0;
  white-space: nowrap;
  display: inline-block;
  width: 150px;
  height: 30px;
  overflow: hidden;
  position: relative;
  background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjREREREREIiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
  background-size: contain;
}
.star-rating i {
  opacity: 0;
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  width: 20%;
  z-index: 1;
  background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjRkZERjg4IiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
  background-size: contain;
}
.star-rating input {
  -moz-appearance: none;
  -webkit-appearance: none;
  opacity: 0;
  display: inline-block;
  width: 20%;
  height: 100%;
  margin: 0;
  padding: 0;
  z-index: 2;
  position: relative;
}
.star-rating input:hover + i,
.star-rating input:checked + i {
  opacity: 1;
}
.star-rating i ~ i {
  width: 40%;
}
.star-rating i ~ i ~ i {
  width: 60%;
}
.star-rating i ~ i ~ i ~ i {
  width: 80%;
}
.star-rating i ~ i ~ i ~ i ~ i {
  width: 100%;
}
/*.choice {
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  text-align: center;
  padding: 20px;
  display: block;
}
*/

</style>
</head>

<body>
<?php
/*$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
if(strpos($ua,'android') == false)
{
//error code goes here
echo '<div style="width:100%; text-align:center;"><h1 class="shopname">Please visit this site on a mobile device!</h1></div>';
exit();
}*/
?>


<?php
$con=mysqli_connect("localhost","cryllsof_khavar","sulfisoxazole","cryllsof_khavar");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
$resid=$_GET['resid'];

$result = mysqli_query($con,"SELECT * FROM shops WHERE ID=$resid");
$row = mysqli_fetch_array($result);
?>
<div>
<div class="imagewrapper">
	<a class="coverlink" data-lightbox="<?php echo  $row['IMAGE']?>" href="./<?php echo  $row['IMAGE']?>" title="<?php echo  $row['NAME']?>"><img id="coverimage" src="<?php echo  $row['IMAGE']?>" style="display:block; margin:auto;"></a>
</div>

<div class="container">
<?php
echo '<div class="stars" style="margin-top:-5px;">';
	$i=0;
	for($i;$i<$row['VOTES'];$i++)
		echo '<span>★</span>';
	for($i;$i<5;$i++)
		echo '<span>☆</span>';
	echo '</div>';
	?>
<p class="shopname"><?php echo $row['NAME']?></p>
<p class="shoplocation"><b>Location:</b>&nbsp;<?php echo $row['LOCATION']?></p>
<p class="menu"><b>Menu:</b>&nbsp;<?php echo $row['MENU']?></p>
<p class="menu" style="text-align:justify;"><b>Description:</b>&nbsp;<br>
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</p>

<p class="shopname"><b>Your Rating:</b><br></p>
<p class="menu">
<form action="rate.php" method="post">
<p class="menu choice">Choose a rating</p>
<span class="star-rating">
  <input type="radio" name="rating" value="1"><i></i>
  <input type="radio" name="rating" value="2"><i></i>
  <input type="radio" name="rating" value="3"><i></i>
  <input type="radio" name="rating" value="4"><i></i>
  <input type="radio" name="rating" value="5"><i></i>
</span>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
$(':radio').change(
  function(){
    $('.choice').text( this.value + ' stars' );
  } 
)
</script>

<br><br>
<input type="hidden" name="resid" value="<?php echo $resid; ?>">
<input type="submit" value="Rate!" style="width:30%; height:36px;">
</form>
</p>

<p class="shopname"><b>Reviews:</b>&nbsp;<br></p>
<p class="menu">
<?php
$con=mysqli_connect("localhost","cryllsof_khavar","sulfisoxazole","cryllsof_khavar");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
$resid=$_GET['resid'];

$result = mysqli_query($con,"SELECT * FROM reviews WHERE ID=$resid ORDER BY NAME ASC");
$count=0;
while($row = mysqli_fetch_array($result))
  {
  echo '<span class="username"><b>';
  echo $row['NAME'];
  echo '</b></span><br>';
  echo '<span class="review">';
  echo $row['REVIEW'];
  echo '</span><br><br>';
  $count = $count + 1;
  }
  if($count == 0)
  {
  echo "No reviews yet. Be the first one to review!";
  echo "<br><br>";
  }
?>
</p>

<p class="shopname"><b>Write a review:</b><br></p>
<p class="menu">
<form action="review.php" method="post">
<span class="menu">Name:</span><br>
<input type="text" name="name" placeholder="Enter name" style="width:96%; height:30px;"><br><br>
<span class="menu">Review:</span><br>
<textarea rows="4" name="review" placeholder="Write your review here in 200 words" style="width:96%;"></textarea><br><br>
<input type="hidden" name="resid" value="<?php echo $resid; ?>">
<input type="submit" value="Submit Review" style="width:50%; height:36px;">
</form>

</p>
</div>
</div>
	</body>
</html>